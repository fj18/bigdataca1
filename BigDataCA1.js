
/*===============================================
1. CREATE DATABASE
===============================================*/
use bigdata_ca1_farwa_javed
show collections;

/*===============================================
2 . INSERT DOCUMENTS
===============================================*/
db.movies.insert({
title : "Fight Club", 
writer : "Chuck Palahniuk",
year : 1999,
actors : ["Brad Pitt","Edward Norton"]
})

db.movies.insert({
title : "Pulp Fiction" ,
writer : "Quentin Tarantino",
year : 1994 ,
actors : ["John Travolta","Uma Thurman"]
})

db.movies.insert({
title : "Inglorious Basterds", 
writer : "Quentin Tarantino", 
year : 2009 ,
actors : ["Brad Pitt","Diane Kruger","Eli Roth"]
})

db.movies.insert({
title : "The Hobbit: An Unexpected Journey",
writer : "J.R.R. Tolkein",
year : 2012 ,
franchise : "The Hobbit"
})

db.movies.insert({
title :"The Hobbit: The Desolation of Smaug",
writer : "J.R.R. Tolkein",
year : 2013,
franchise : "The Hobbit"
})

db.movies.insert({
title : "The Hobbit: The Battle of the Five Armies",
writer : "J.R.R. Tolkein", 
year : 2012,
franchise : "The Hobbit", 
synopsis : "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."
})

db.movies.insert({
title : "Pee Wee Herman's Big Adventure"
})

db.movies.insert({
title : "Avatar"
})

/*===================================================
3 .  QUERY/FIND DOCUMENTS
===================================================*/
//1) Get all documents
db.movies.find().pretty()

//2)Get all documents with writer set to "Quentin Tarantino" 
db.movies.find({
writer:"Quentin Tarantino"
})


//3)Get all documents where actors include "Brad Pitt" 
db.movies.find({
actors:"Brad Pitt"
})

//4)Get all documents with franchise set to "The Hobbit" 
db.movies.find({
franchise:"The Hobbit"
}).pretty()

//5)Get all movies released in the 90s 
db.movies.find({ $where: "this.year > 1990 && this.year <2000"
}).pretty()
db.movies.find({$and: [{year: {$gt: 1990}}, {year: {$lt: 2000}}]})

//6)Get all movies released before the year 2000 or after 2010
db.movies.find({ $where: "this.year < 2000 || this.year > 2010"
}).pretty()
db.movies.find({$or: [{year: {$gt: 2010}}, {year: {$lt: 2000}}]})

/*=======================================================
4 . UPDATE DOCUMENT
=======================================================*/
1)add a synopsis to The Hobbit: An Unexpected Journey
db.movies.update({"title": "The Hobbit: An Unexpected Journey"},
{$set: {"synopsis":"The Hobbit: An Unexpected Journey : A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug"}}
);

2)add a synopsis to "The Hobbit: The Desolation of Smaug" 
db.movies.update({"title": "The Hobbit: The Desolation of Smaug"},
{$set: {"synopsis":"The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."}}
);

3)add an actor named "Samuel L. Jackson" to the movie "Pulp Fiction"
db.movies.update({title: "Pulp Fiction"}, {$push: {actors: "Samuel L. Jackson"}})
db.movies.find({actors:"Samuel L. Jackson"}).pretty()

/*===================================================
5. TEXT SEARCH
===================================================*/

//1)Find all movies that have a synopsis that contains the word "Bilbo" 
db.movies.find( {synopsis: /Bilbo/}).pretty()
db.movies.find( {synopsis: /Bilbo/},{title: 1}
).pretty()

//2)Find all movies that have a synopsis that contains the word "Gandalf" 
db.movies.find( {
synopsis: /Gandalf/
}).pretty()

//3). find all movies that have a synopsis that contains the word "Bilbo" and not the word "Gandalf" 
db.movies.find({
$and: [{synopsis:{$not: /Gandalf/}} ,
{synopsis:/Bilbo/}
]}).pretty()

//4)find all movies that have a synopsis that contains the word "dwarves" or "hobbit" 
db.movies.find({
$or: [{synopsis: /dwarves/} ,
{synopsis:/hobbit/}
]}).pretty()

//5)find all movies that have a synopsis that contains the word "gold" and "dragon"
db.movies.find({
$and: [{synopsis: /gold/} ,
{synopsis:/dragon/}
]}).pretty()

/*==================================================================================
6. DELETE DOCUMENTS
==================================================================================*/
//1. delete the movie "Pee Wee Herman's Big Adventure"
db.movies.remove({
title: "Pee Wee Herman's Big Adventure"
})

//2. delete the movie "Avatar"
db.movies.remove({
title: "Avatar"
})

/*====================================================================================
7. RELATIONSHIPS
====================================================================================*/

//1)Insert the following documents into a `users` collection

db.users.insert({
username : "GoodGuyGreg",
first_name : "Good Guy", 
last_name : "Greg"
})

db.users.insert({
username : "ScumbagSteve",
full_name :{
first : "Scumbag",
last : "Steve"
}})

//2)Insert the following documents into a `posts` collection

db.posts.insert({
username : "GoodGuyGreg", 
title : "Passes out at party", 
body : "Wakes up early and cleans house"
})

db.posts.insert({
username : "GoodGuyGreg",
title : "Steals your identity", 
body :"Raises your credit score"
})

db.posts.insert({
username : "GoodGuyGreg",
title : "Reports a bug in your code", 
body : "Sends you a Pull Request"
})

db.posts.insert({
username : "ScumbagSteve", 
title : "Borrows something",
body : "Sells it"
})

db.posts.insert({
username : "ScumbagSteve", 
title : "Borrows everything", 
body : "The end"
})

db.posts.insert({
username : "ScumbagSteve",
title : "Forks your repo on github ",
body : "Sets to private"
})

3)Insert the following documents into a `comments` collection
db.comments.insert({
username : "GoodGuyGreg", 
comment : "Hope you got a good deal!", 
post : ObjectId("59f347d4a98ae6dd7fc5226c")
})

db.comments.insert({
username : "GoodGuyGreg",
comment : "What's mine is yours!",
post : ObjectId("59f34816a98ae6dd7fc5226d")
})

db.comments.insert({
username : "GoodGuyGreg",
comment : "Don't violate the licensing agreement!", 
post :  ObjectId("59f3484fa98ae6dd7fc5226e")
})

db.comments.insert({
username : "ScumbagSteve",
comment : "It still isn't clean",
post :  ObjectId("59f34712a98ae6dd7fc52269")
})

db.comments.insert({
username : "ScumbagSteve",
comment : "Denied your PR cause I found a hack", 
post : ObjectId("59f3479fa98ae6dd7fc5226b")
})

/*==========================================================
8. QUERING RELATED COLLECTIONS
==========================================================*/
//1) find all users 
db.users.find().pretty()

//2) find all posts 
db.posts.find().pretty()

//3)find all posts that was authored by "GoodGuyGreg" 
db.posts.find({username: "GoodGuyGreg"}).pretty()

//4)find all posts that was authored by "ScumbagSteve" 
db.posts.find({username: "ScumbagSteve"}).pretty()

//5)find all comments 
db.comments.find().pretty()

//6)find all comments that was authored by "GoodGuyGreg" 
db.comments.find({username: "GoodGuyGreg"}).pretty()

//7)find all comments that was authored by "ScumbagSteve" 
db.comments.find({username: "ScumbagSteve"}).pretty()



