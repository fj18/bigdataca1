
/*===========================================
1. CREATE DATABASE
===========================================*/
create database bigdata
\c bigdata

/*==========================================
2. INSERT DOCUMENTS
==========================================*/
 
CREATE TABLE movies(
title text,
writer CHAR(20),
year int,
actors text[],
franchise text,
synopsis text
);

INSERT INTO movies(title,writer,year,actors) 
VALUES('Fight Club','Chuck Palahniuk',1999,'{"Brad Pitt","Edward Norton"}'
);

INSERT INTO movies(title,writer,year,actors) 
VALUES('Pulp Fiction','Quentin Tarantino',1994,'{"John Travolta","Uma Thurman"}'
);

INSERT INTO movies(title, writer,year,actors) 
VALUES('Inglorious Basterds','Quentin Tarantino',2009,'{"Brad Pitt","Diane Kruger","Eli Roth"}'
);

INSERT INTO movies(title,writer,year,franchise)
VALUES('The Hobbit: An Unexpected Journey','J.R.R. Tolkein',2012,'The Hobbit');

INSERT INTO movies(title,writer,year,franchise)
VALUES('The Hobbit: The Desolation of Smaug','J.R.R. Tolkein',2013,'The Hobbit');

INSERT INTO movies(title,writer,year,franchise,synopsis)
VALUES('The Hobbit: The Battle of the Five Armies','J.R.R. Tolkein',2012,'The Hobbit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.
');

INSERT INTO movies(title)
VALUES('Pee Wee Herman''s Big Adventure'
);

INSERT INTO movies(title)
VALUES('Avatar');

/*====================================================
3. QUERY / FIND DOCUMENTS
====================================================*/
--Query the `movies` table to
--1) get all Rows 
SELECT * from movies;

--2)get all Rows with `writer` set to "Quentin Tarantino" 
SELECT * FROM movies WHERE writer = 'Quentin Tarantino';

--3)Get all rows where `actors` include "Brad Pitt" 
SELECT * FROM movies WHERE 'Brad Pitt' = ANY(actors);

--4)Get all rows with `franchise` set to "The Hobbit" 
SELECT * FROM movies where franchise = 'The Hobbit';

--5)Get all movies released in the 90s 
SELECT * FROM movies WHERE year BETWEEN 1990 AND 1999;

--6)Get all movies released before the year 2000 or after 2010
SELECT * FROM movies WHERE year < 2000 OR year > 2010;

/*======================================================
4. UPDATE DOCUMENTS
======================================================*/

--1)Add a synopsis to "The Hobbit: An Unexpected Journey" : "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug." 
UPDATE movies SET synopsis = 'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.'
WHERE title = 'The Hobbit: An Unexpected Journey';

--2)Add a synopsis to "The Hobbit: The Desolation of Smaug" : "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring." 
UPDATE movies SET synopsis ='The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug.
Bilbo Baggins is in possession of a mysterious and magical ring'
WHERE title = 'The Hobbit: The Desolation of Smaug';

--3)Add an actor named "Samuel L. Jackson" to the movie "Pulp Fiction"
UPDATE movies SET actors = array_append(actors, 'Samuel L. Jackson')
WHERE title = 'Pulp Fiction';

/*======================================================
5. Text Search
======================================================*/
--1)Find all movies that have a synopsis that contains the word "Bilbo" 
SELECT * FROM movies WHERE synopsis LIKE '%Bilbo%';

--2)Find all movies that have a synopsis that contains the word "Gandalf" 
SELECT * FROM movies WHERE synopsis LIKE '%Gandalf%';

--3)Find all movies that have a synopsis that contains the word "Bilbo" and not the word "Gandalf" 
SELECT * FROM movies WHERE synopsis ILIKE '%Bilbo%' AND synopsis NOT ILIKE '%Gandalf%';

--4)Find all movies that have a synopsis that contains the word "dwarves" or "hobbit" 
SELECT * FROM movies WHERE synopsis ILIKE '%dwarves%' OR synopsis ILIKE '%hobbit%';

--5)find all movies that have a synopsis that contains the word "gold" and "dragon"
SELECT * FROM movies WHERE synopsis ILIKE '%gold%' AND synopsis ILIKE '%dragon%';

/*======================================================
6. DELETE 
======================================================*/

1)delete the movie "Pee Wee Herman's Big Adventure"
DELETE FROM movies WHERE title = 'Pee Wee Herman''s Big Adventure';

2)delete the movie "Avatar"
DELETE FROM movies WHERE title = 'Avatar';

/*=======================================================
7. RELATIONSHIPS
=======================================================*/
1)Insert the following into a `users` Table

CREATE TABLE users(user_id SERIAL PRIMARY KEY,username text,
first_name text,last_name text);

INSERT INTO users(username,first_name,last_name) VALUES('GoodGuyGreg','Good Guy','Greg');

INSERT INTO users(username,first_name,last_name) VALUES('ScumbagSteve','Scumbag','Steve');

2)Insert the following documents into a `posts` Table

CREATE TABLE posts(
post_id SERIAL PRIMARY KEY,
username text,
title text,
body text
);

INSERT INTO posts(username,title,body) VALUES('GoodGuyGreg','Passes out at party','Wakes up early and cleans house');

INSERT INTO posts(username,title,body) VALUES('GoodGuyGreg','Steals your identity','Raises your credit score');

INSERT INTO posts(username,title,body) VALUES('GoodGuyGreg','Reports a bug in your code','Sends you a Pull Request');

INSERT INTO posts(username,title,body) VALUES('ScumbagSteve','Borrows something','Sells it');

INSERT INTO posts(username,title,body) VALUES('ScumbagSteve','Borrows everything','The end');

INSERT INTO posts(username,title,body) VALUES('ScumbagSteve','Forks your repo on github','Sets to private');


3)Insert the following documents into a 'comments' TABLE
CREATE TABLE comments(
comment_id SERIAL PRIMARY KEY,
username text,
comment text,
post_id int REFERENCES posts
);

INSERT INTO comments(username,post_id,comment) VALUES('GoodGuyGreg',4,'Borrows something');

INSERT INTO comments(username,post_id,comment) VALUES('GoodGuyGreg',5,'Borrows everything');

INSERT INTO comments(username,post_id,comment) VALUES('GoodGuyGreg',6,'Don"t violate the licensing agreement!');

INSERT INTO comments(username,post_id,comment) VALUES('ScumbagSteve',1,'It still isn"t clean');

INSERT INTO comments(username,post_id,comment) VALUES('ScumbagSteve',3,'Denied your PR cause I found a hack ');

/*=====================================================
8. QUERYING RELATED TABLES
=====================================================*/
1)Find all users 
SELECT * FROM users;

2)Find all posts 
SELECT * FROM posts;

3)Find all posts that was authored by "GoodGuyGreg" 
SELECT * FROM posts WHERE username = 'GoodGuyGreg';

4)Find all posts that was authored by "ScumbagSteve" 
SELECT * FROM posts WHERE username  = 'ScumbagSteve';

5)Find all comments 
SELECT * FROM comments;

6)Find all comments that was authored by "GoodGuyGreg" 
SELECT * FROM comments WHERE username = 'GoodGuyGreg';

7)Find all comments that was authored by "ScumbagSteve" 
SELECT * from comments WHERE username = 'ScumbagSteve';

8)Find all comments belonging to the post "Reports a bug in your code"
SELECT * from comments com,posts post WHERE com.post_id = post.post_id AND post.title='Reports a bug in your code';







